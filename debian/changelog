libgraphics-color-perl (0.31-2) unstable; urgency=medium

  [ Salvatore Bonaccorso ]
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ gregor herrmann ]
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.
  * Remove Jonathan Yu from Uploaders. Thanks for your work!
  * Remove Nathan Handler from Uploaders. Thanks for your work!

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ gregor herrmann ]
  * debian/watch: use uscan version 4.

  [ Debian Janitor ]
  * Bump debhelper from deprecated 8 to 12.
  * Set debhelper-compat version in Build-Depends.
  * Set upstream metadata fields: Bug-Submit.
  * Remove obsolete fields Contact, Name from debian/upstream/metadata
    (already present in machine-readable debian/copyright).
  * Remove constraints unnecessary since stretch:
    + Build-Depends-Indep: Drop versioned constraint on libmoosex-aliases-perl.
    + libgraphics-color-perl: Drop versioned constraint on
      libmoosex-aliases-perl in Depends.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Tue, 14 Jun 2022 22:09:17 +0100

libgraphics-color-perl (0.31-1.1) unstable; urgency=medium

  * Non maintainer upload by the Reproducible Builds team.
  * No source change upload to rebuild on buildd with .buildinfo files.

 -- Holger Levsen <holger@debian.org>  Tue, 05 Jan 2021 13:17:51 +0100

libgraphics-color-perl (0.31-1) unstable; urgency=low

  [ gregor herrmann ]
  * debian/control: update {versioned,alternative} (build) dependencies.

  [ Nathan Handler ]
  * Email change: Nathan Handler -> nhandler@debian.org

  [ Salvatore Bonaccorso ]
  * Change Vcs-Git to canonical URI (git://anonscm.debian.org)
  * Change search.cpan.org based URIs to metacpan.org based URIs

  [ Axel Beckert ]
  * debian/copyright: migrate pre-1.0 format to 1.0 using "cme fix dpkg-
    copyright"

  [ gregor herrmann ]
  * Strip trailing slash from metacpan URLs.

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend

  [ Florian Schlichting ]
  * Add debian/upstream/metadata
  * Import upstream version 0.31
  * Update copyright years
  * Email change: Florian Schlichting -> fsfs@debian.org
  * Forward manpage-has-bad-whatis-entry.patch
  * Declare compliance with Debian Policy 3.9.6 (no changes necessary)
  * Add autopkgtest header

 -- Florian Schlichting <fsfs@debian.org>  Mon, 04 May 2015 21:50:31 +0200

libgraphics-color-perl (0.29-1) unstable; urgency=low

  [ Florian Schlichting ]
  * Imported Upstream version 0.29
  * bump debian/compat (no changes)
  * debian/copyright:
    - new copyright holder as of 2011
    - inc/Module/* removed
    - add myself to debian/*
  * bump Standards-Version (directly link GPL-1)
  * patch two manpages adding whatis descriptions
  * add myself to Uploaders
  * make dependency on libmoosex-aliases-perl versioned (cf RT#70193)

  [ Nathan Handler ]
  * New upstream release
  * debian/copyright:
    - Update to use latest revision of DEP5
    - Add myself to debian/* copyright
  * debian/control:
    - Add myself to list of Uploaders
    - Bump Standards-Version to 3.8.4 (no changes)
  * debian/source/format: Use dpkg-source 3.0 format

  [ Ansgar Burchardt ]
  * debian/control: Convert Vcs-* fields to Git.

  [ Salvatore Bonaccorso ]
  * debian/copyright: Replace DEP5 Format-Specification URL from
    svn.debian.org to anonscm.debian.org URL.

 -- Florian Schlichting <fschlich@zedat.fu-berlin.de>  Thu, 25 Aug 2011 12:08:36 +0000

libgraphics-color-perl (0.27-1) unstable; urgency=low

  * Initial Release (Closes: #563259)

 -- Jonathan Yu <jawnsy@cpan.org>  Fri, 15 Jan 2010 20:05:55 -0500
